package com.a3d.examen_12550461;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    private SQLiteDatabase sqlite;
    private TextView menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        menu = (TextView) findViewById(R.id.menu);

        File fRuta = getApplication().getExternalFilesDir(null);
        String ruta = fRuta + "/" + "examen";

        sqlite = SQLiteDatabase.openDatabase(ruta, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        sqlite.execSQL("DROP TABLE IF EXISTS menu");
        sqlite.execSQL("CREATE TABLE IF NOT EXISTS menu(" +
                        "id            INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "name          TEXT," +
                        "price         TEXT," +
                        "description   TEXT)");
        readXml();
    }

    public void readXml(){
        String text;
        String name = null;
        String price = null;
        String description = null;
        try{
            XmlPullParser parser = getResources().getXml(R.xml.examen);
            int eventType = parser.getEventType();
            String tag = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        tag = parser.getName();
                        break;
                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        if (tag != null) {
                            switch (tag) {
                                case "name":
                                    name = text;
                                    break;
                                case "price":
                                    price = text;
                                    break;
                                case "description":
                                    description = text;
                                    break;
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equals("food")) {
                            sqlite.execSQL("INSERT INTO menu(name, price, description) VALUES(?, ?, ?)", new String[]{name, price, description});
                            menu.append('\n' + name + '\n' + description + '\n' + price);
                        }
                        break;
                }
                eventType = parser.next();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
